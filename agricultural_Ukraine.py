
import os
import pandas as pd
import numpy as np
from pandas import Series, DataFrame

#get data from csv 
df_raw= pd.read_csv('/Users/dasha/Downloads/fao_data_crops_data.csv')

#check null items
df_raw.isnull().sum()

#replace nan, for value = 0
df_raw['element'].fillna("No_element",inplace=True)
df_raw['year'].fillna("No_year",inplace=True)
df_raw['value_footnotes'].fillna("No_value_footnotes",inplace=True)
df_raw['unit'].fillna("No_unit",inplace=True)
df_raw['value'].fillna(0,inplace=True)


#get data from csv (dictionaty EU country)
df_country_europe= pd.read_csv('/Users/dasha/Downloads/country_europe_dictionary.csv',sep = ';', encoding='UTF-8')

#merge 2 dataframe
df_merge = pd.merge(df_raw, df_country_europe, how = "left", left_on = ['country_or_area'], right_on = ['country'])

#replace nan
df_merge['area'].fillna("Not_Europe",inplace=True)

#choose only eu country
df_eu = df_merge[df_merge['area'].str.contains(' +')]


df_eu_sum = df_eu.groupby(['country_or_area']).sum(['value'])

#choose only Ukraine
df_Ukraine = df_eu_sum[df_eu['country_or_area']=='Ukraine']

#check value by year
df_Ukraine_group_year= df_Ukraine['value'].groupby(df_Ukraine['year']).mean()


#group by main rows
df_Ukraine_sum_all = df_Ukraine.groupby(['year','element','unit','category','value_footnotes']).sum()


df_Ukraine_sum_all.to_csv(r'/Users/dasha/Downloads/df_Ukraine_sum_all.csv', encoding='utf-8')


#percent of category
df_Ukraine_count_category = df_Ukraine['category'].value_counts(normalize=True)*100

